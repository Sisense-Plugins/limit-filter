
prism.run([function(){

	//	Define the controller for the modal window
	mod.controller('limitFilterController',
        ['$scope', 'widget-editor.services.$popper', function ($scope, $popper) {

		//	Define the default settings
		var defaultSettings = {
			direction: 'Top',
			count: 1
		}

		var options = {
			directions: ['Top','Bottom'],
			count: [1,10]
		}

    	//	Grab the scope
    	var myScope = $scope;

        //  Function to create a key for a given panel item

        function getKey(obj){
            
            var myString = obj.jaql.dim + (obj.jaql.level ? '-' + obj.jaql.level : '');
            var hash = 0, i, chr, len;            
            for (i = 0, len = myString.length; i < len; i++) {
                chr   = myString.charCodeAt(i);
                hash  = ((hash << 5) - hash) + chr;
                hash |= 0; // Convert to 32bit integer
            }

            return hash
        }

    	//	Assign default values to settings
    	if (typeof myScope.settings == "undefined"){

            myScope.options = options;

            //  Look for an existing limit filter
            var key = getKey(myScope.selectedItem),
                match = myScope.widget.options.limitFilters[key];

            if (match){
                //  Already added a limit filter
                myScope.settings = {};
                if (typeof match.bottom == "undefined") {
                    myScope.settings.direction = 'top';
                    myScope.settings.count = match.top;
                } else {
                    myScope.settings.direction = 'bottom';
                    myScope.settings.count = match.bottom;
                }                
                myScope.byTitle = { html: '   ' + match.by.column, plain: match.by.column};
                myScope.closeTitle = { html: 'Delete', plain: ''};
                myScope.newJaql = match.by;
            } else {
                //  This is a new limit filter
        	    myScope.settings = defaultSettings;
                myScope.byTitle = { html: '', plain: '' };
                myScope.closeTitle = { html: 'Cancel', plain: '' };
                myScope.newJaql = {};
            }
    	}

        //  Function to create the new filter jaql
        function createFilterJaql(selectionJaql){

            selectionJaql.filter = {
                
            }
        }

        //  Function to set the filter as a selection
        function setFieldsArr(fieldsToSet,newjaql){
            debugger
            /*
            var fields =  $$get($scope.widget.options,fieldsToSet) || [];
            newjaql.title = newjaql.title || newjaql.column;
            fields.push(newjaql);
            $$set($scope.widget.options,fieldsToSet,fields);
            */
        }

        

        //  Function to handle when a user clicks the select fields button
        myScope.addItem = function (ev,period) {

            //  Get the period
            period = period || null;

            //  Prevent running if in view only mode
            if (myScope.viewonly) {
                return;
            }

            //  Add some properties to the scope
            myScope.isFiltersPanel = false;
            myScope.isCustomPopup = true;
            myScope.isMeasure = false;            

            //  Define the popper attributes
            var popperAtt = {
                dimensions: true,
                measures:true 
            };

            //  Function that runs when the popup is opened
            function openPopper(){
                //  Find the popup and add a class
                $(".uc-db-popup.sis-popup-window.popup-host.simple").addClass("designPanel");

                //  Apply the phase if necessary
                if (!myScope.$root.$$phase) {
                    myScope.$apply(function () {});
                }
            }

            //  Function that runs when a field is selected
            function selectItem(newJaql) {                    
                    //  Save the selection to the scope
                    myScope.newJaql = newJaql;

                    //  Make sure the selection text is visible
                    myScope.byTitle.html = "   " + newJaql.column;
                    myScope.byTitle.text = newJaql.column;                    

                    //  Remove the popup
                    $(".uc-db-popup.sis-popup-window.popup-host.simple").remove();
                    $(".popup-overlay").remove();            
            }
            
            //  Open the popup
            var close = $popper.popJaql(
                myScope,                    //  Scope
                popperAtt,                  //  Popup attributes
                null,                       //
                ev,                         //  Event
                openPopper,                 //  Function for when the popup opens
                null,                       //  
                selectItem,                 //  Function for when a selection is made
                'we.breads.addfield'        //  Some class???
            );
        };

    	//	Function to close the window
        myScope.setFilter = function(){
            
            //  Init the filter object
            var filter = {};

            //  Define rank direction
            filter[myScope.settings.direction.toLowerCase()] = parseInt(myScope.settings.count);

            //  How are we ranking by?
            filter.by = myScope.newJaql;

            //  Decide whether to use min or max as the aggregation
            if(myScope.settings.direction.toLowerCase() == 'top'){
                filter.by.agg = "max";
            } else {
                filter.by.agg = "min";
            }

            //  HANDLE measures     //

            //  Get the key for the selected panel item
            var key = getKey(myScope.selectedItem);

            //  Save the filter to the widget
            myScope.widget.options.limitFilters[key] = filter;

            //  Update the widget (with query)
            //$scope.widget.changesMade();
            myScope.widget.refresh();

        	//	Run the close function
        	this.finished()
        };

        //  Function to remove the limit filter
        myScope.removeFilter = function(event){

            //  Figure out what dim is selected
            var key = getKey(this.selectedItem);

            //  Remove the filter
            if (this.widget.options.limitFilters){
                delete this.widget.options.limitFilters[key]
            }        

            //  Run the close function
            myScope.widget.refresh();
            this.finished()

        }
		    
        //	Function to close the window
        myScope.close = function(){

        	//	Run the close function
        	this.finished()
        };
	}])

}])