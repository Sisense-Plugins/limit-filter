
//	Initialize the formatting options
prism.run([function() {	

	//	Define allowed chart types
	var supportedChartTypes = ["pivot","table"];
	
	//	Options
	var myProperty = 'advancedFilter';
	var headerMenuCaption = "Limit Filter";
	var noFilterCaption = "Select a Field";
	var clearFilterCaption = "Clear Limit Filter";
	var none = false;
		

	//////////////////////////////////////
	//	Widget Specific Functions		//
	//////////////////////////////////////	

	//	Function to update the query before sending to Elasticube
	function updateQuery(widget,args){	

		//	Add hook for the time formatting
		var shouldInit = widgetIsSupported(args.widget.type);
		if (shouldInit) {

			//	Sample filter structure
			var jaqlFilter = {
				"filter": {
					"top": 1,
					"by": {
						"dim": "[Sales.OrderDate1 (Calendar)]",
						"level": "days",
						"agg": "max"
					}
				}
			}

			//	Loop through each metadata item and check to see if we need to add a filter
			$.each(args.query.metadata,function(){
				
				//	Calculate the key to look for		
				var thisKey = getKey(this),
					match = $$get(args.widget.options.limitFilters,thisKey);

				//	If a match is found and this isn't a filter, add the dimension filter
				if(match && (this.panel !== "scope")){
					this.jaql.filter = match;
				}
			})

			//	Clean up the limitFilters object, in case a dim was deleted
			for (key in args.widget.options.limitFilters){

				//	Loop through the metadata to see if there is a match
				var matchFound = false;
				for (var i=0; i<args.query.metadata.length; i++){
					var thisKey = getKey(args.query.metadata[i]);
					var found = (thisKey == key) && (args.query.metadata[i].panel !== "scope");
					if (found){
						matchFound = true;
					}
				}

				//	Not used anymore, remove the reference
				if (!matchFound){
					delete args.widget.options.limitFilters[key];
				}
			}
		
		}

		//	Return the query
		return args.query
	}
	
	//////////////////////////////////
	//	Utility Functions			//
	//////////////////////////////////

	//  Function to create a key for a given panel item
    function getKey(obj){
        //return obj.jaql.dim + (obj.jaql.level ? '-' + obj.jaql.level : '');
        var myString = obj.jaql.dim + (obj.jaql.level ? '-' + obj.jaql.level : '');
        var hash = 0, i, chr, len;            
        for (i = 0, len = myString.length; i < len; i++) {
            chr   = myString.charCodeAt(i);
            hash  = ((hash << 5) - hash) + chr;
            hash |= 0; // Convert to 32bit integer
        }
        return hash 
    }

	//	function to find the row items from a widget
	function getItems(widget,section) {
		var valueItems = $.grep(widget.metadata.panels, function(w){
			return w.title===section;
		}).first();
		return valueItems;
	}	

	//	Function to determine if this chart type is supported
	function widgetIsSupported(type){
		if (supportedChartTypes.indexOf(type) >= 0) {
			return true;
		} else {
			return false;
		}
	}

	//	Function to determine if the menu option should be added
	function canEnable(scope, args){
		
		//	Assume true by default
		var result = true;				
		try {

			//	Check the name
			var name = args.settings.name;
			if (name !== "widget-metadataitem") {
				result = false;
			}

			//	Don't show for filters
			var filterClass = "we-filter-menu-popup";
			if (args.ui.css === filterClass){
				//result = false;
			}
						
		} catch(e) {
			result = false;
		}

		//	Return result
		return result;
	}

	//	Function to show the data picker popup
	//	showPicker(scope, widget, this.clickedElement[0], this.item);
	function showPicker(scope,widget, element, item){

		//	Create a copy of the scope
		//var newScope = scope.targetScope.$new();
		var newScope = scope.targetScope;

		//	Add custom attributes to the new scope
        newScope.name = "af-" + item.jaql.dim;
        newScope.item = item;
        newScope.widget = widget
        newScope.isFiltersPanel = false;
        newScope.isCustomPopup = true;
        newScope.isMeasure = false;

        //	Define what field types are displayed
        var ualTypes = { 
        	dimensions: true,
        	measures: false 
        };

        //	Existing Jaql to pass in (used when adding custom jaql to filters)
        var existingJaql = null;

        //	triggering element event
        var eventElement = element;

        //	callback function for pre-cancel
        var userCanceled = function () {
        
            //	Apply the scope
            if (!newScope.$$phase){
            	newScope.$apply()
            }
        }

        //	Callback function after selection
        var fieldSelected = function (newjaql) {
        	debugger
            //	Do something with the jaql


            //	???
            $(".uc-db-popup.sis-popup-window.popup-host.simple").remove();
            $(".popup-overlay").remove();

            //	Requery the widget
            //newScope.widget.refresh();
        }

        //	Title key for localized label lookup
        var titleKey = 'we.breads.addfield';

        var $popper = prism.$injector.get('widget-editor.services.$popper');

        //	Instantiate the popup
        var close = $popper.popJaql(newScope, ualTypes, existingJaql, eventElement, userCanceled, null, fieldSelected, titleKey);        
	}


	//////////////////////////////////
	//	Modal Window Functions		//
	//////////////////////////////////

	


	//////////////////////////////////
	//	Initialization Functions	//
	//////////////////////////////////

	// Registering dashboard/ widget creation in order to perform drilling
	prism.on("dashboardloaded", function (e, args) {
		args.dashboard.on("widgetinitialized", onWidgetInitialized);
		args.dashboard.on("widgetbeforequery", updateQuery)
	});

	// register widget events upon initialization
	function onWidgetInitialized(dashboard, args) {
		//	Hooking to ready/destroyed events
		args.widget.on("destroyed", onWidgetDestroyed);		
		//	Add hook for the time formatting
		var shouldInit = widgetIsSupported(args.widget.type);
		if (shouldInit) {
			//	Add the hook on buildquery
			//args.widget.on("beforequery", updateQuery);
			//	Make sure the widget has a limitFilters option
			if (typeof args.widget.options.limitFilters == "undefined"){
				args.widget.options.limitFilters = {}
			}
		}
	}

	// unregistering widget events
	function onWidgetDestroyed(widget, args) {
		widget.off("destroyed", onWidgetDestroyed);
	}

	//	Create Options for the quadrant analysis
	prism.on("beforemenu", function (scope, args) {

		//	Can we show the option?	
		var shouldEnable = canEnable(scope, args);				

		//	Add the option to enable the regression line
		if (shouldEnable) {			
			
			//	Get the item and widget
			var selectedItem = args.settings.item,
				widget = scope.currentScope.widget;

			//	Look for a saved setting
			var key = getKey(selectedItem),
				existingFilter = $$get(widget.options.limitFilters,key),
				hasExisting = (typeof existingFilter !== "undefined");

			//	Function the runs when an item is picked
			var showPopup = function(){

				//	Get the scope
				//var myScope = prism.$ngscope;
				var myScope = scope.targetScope;
				myScope.widget = this.widget;
				myScope.selectedItem = this.item;
				
				//	Get the dom injector
				var myDom = prism.$injector.get('ux-controls.services.$dom');

				//	Define the modal window's options
				var options = {
					scope: { 
						in: myScope 
					},
					templateUrl: '/plugins/limitFilter/popupTemplate.html'				
				};

				//	Define the ui options			
				var ui = {};

				//	Add the modal window			
				myDom.modal(options,ui);
				
				//	Apply the scope, but make sure  				
				if (!myScope.$$phase){
					myScope.$apply();
				}
			}

			//	Function to clear an existing filter
			var clearFilter = function(){

			}

			//	Create settings menu
			var separator = {
				type: 'separator'
			};
			var newItem = {
				caption: headerMenuCaption,
				size: 'xl',
				type: 'check',
				checked: hasExisting,
				widget: widget,
				item: args.settings.item,
				execute: showPopup,
				closing: true
			};

			//	Add options to the menu			
			args.settings.items.push(separator);	
			args.settings.items.push(newItem);	

		}
		
	});

}]);