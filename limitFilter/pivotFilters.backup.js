
//	Initialize the formatting options
prism.run([function() {	

	//	Define allowed chart types
	var supportedChartTypes = ["pivot"];
	
	//	Options
	var myProperty = 'advancedFilter';
	var headerMenuCaption = "Advanced Filter";
	var noFilterCaption = "Select a Field";
	var clearFilterCaption = "Clear Filter";
	var none = false;
		

	//////////////////////////////////////
	//	Widget Specific Functions		//
	//////////////////////////////////////	

	//	Function to update the query before sending to Elasticube
	function updateQuery(widget,query){		

		return query
	}
	
	//////////////////////////////////
	//	Utility Functions			//
	//////////////////////////////////

	//	function to find the row items from a widget
	function getItems(widget,section) {
		var valueItems = $.grep(widget.metadata.panels, function(w){
			return w.title===section;
		}).first();
		return valueItems;
	}	

	//	Function to determine if this chart type is supported
	function widgetIsSupported(type){
		if (supportedChartTypes.indexOf(type) >= 0) {
			return true;
		} else {
			return false;
		}
	}

	//	Function to determine if the menu option should be added
	function canEnable(scope, args){
		
		//	Assume true by default
		var result = true;				
		try {

			//	Check the name
			var name = args.settings.name;
			if (name !== "widget-metadataitem") {
				result = false;
			}

			//	Don't show for filters
			var filterClass = "we-filter-menu-popup";
			if (args.ui.css === filterClass){
				result = false;
			}
						
		} catch(e) {
			result = false;
		}

		//	Return result
		return result;
	}

	//	Function to show the data picker popup
	//	showPicker(scope, widget, this.clickedElement[0], this.item);
	function showPicker(scope,widget, element, item){

		//	Create a copy of the scope
		//var newScope = scope.targetScope.$new();
		var newScope = scope.targetScope;

		//	Add custom attributes to the new scope
        newScope.name = "af-" + item.jaql.dim;
        newScope.item = item;
        newScope.widget = widget
        newScope.isFiltersPanel = false;
        newScope.isCustomPopup = true;
        newScope.isMeasure = false;

        //	Define what field types are displayed
        var ualTypes = { 
        	dimensions: true,
        	measures: false 
        };

        //	Existing Jaql to pass in (used when adding custom jaql to filters)
        var existingJaql = null;

        //	triggering element event
        var eventElement = element;

        //	callback function for pre-cancel
        var userCanceled = function () {
        
            //	Apply the scope
            if (!newScope.$$phase){
            	newScope.$apply()
            }
        }

        //	Callback function after selection
        var fieldSelected = function (newjaql) {
        	debugger
            //	Do something with the jaql


            //	???
            $(".uc-db-popup.sis-popup-window.popup-host.simple").remove();
            $(".popup-overlay").remove();

            //	Requery the widget
            //newScope.widget.refresh();
        }

        //	Title key for localized label lookup
        var titleKey = 'we.breads.addfield';

        var $popper = prism.$injector.get('widget-editor.services.$popper');

        //	Instantiate the popup
        var close = $popper.popJaql(newScope, ualTypes, existingJaql, eventElement, userCanceled, null, fieldSelected, titleKey);        
	}

	//	Popjaql function from metaPickerCtrl.js
	function popJaql(scope, jaql, srcEl, preCancel, postCancel, successCallback, prevent, options) {

		var $url = prism.$injector.get('base.services.$url');
		var $dom = prism.$injector.get('ux-controls.services.$dom');

        var defaults = {
            title: null,
            dimensions: false,
            measures: false,
            filter: false,
        };
        var options = $.extend(defaults, options);
        var ualTypes = options;

        // New isolated scope for the databrowser popup
        var newScope = scope.$new(true);

        newScope.name = "af-" + scope.item.jaql.dim;
        newScope.item = scope.item;
        newScope.widget = scope.widget
        newScope.isFiltersPanel = false;
        newScope.isCustomPopup = true;
        newScope.isMeasure = false;

        var cancelCore = function (ev) {

            if (newScope) {
                newScope.finished(ev);
            }
        }

        // Popup Editor "in" params
        newScope.in = {
            cancel: function () {

                if (preCancel) {
                    preCancel.call();
                }

                // core.
                cancelCore();

                if (postCancel) {
                    postCancel.call();
                }
            },
            hideBar: true,
            title: options.title,
            datasource: scope.datasource
        };

        newScope.unpop = cancelCore;

        // Databrowser template & controller        
        newScope.templateUrl = $url.template('metaPicker', 'eucalyptus');
        var controller = 'eucalyptus.controllers.metaPickerController';

        // If jaql was passed in, we're in edit (filter) mode and we open a different window
        if (jaql) {

            newScope.filter = jaql;
        }

        // available types.
        newScope.allowDim = ualTypes.dimensions;
        newScope.allowMeasure = ualTypes.measures;
        newScope.allowFilter = ualTypes.filter;
        newScope.selectOnly = ualTypes.filter && !ualTypes.dimensions && !ualTypes.measures;
        newScope.hideActionText = false; // ???

        newScope.prevent = prevent;

        newScope.$callbacks = {

            cancel: function(){

                if (preCancel) {
                    preCancel();
                }

                cancelCore();

            },

            ok: successCallback

        };

        newScope.$shared = {
            $g : {}
        };

        function shouldCloseOnBlur(){
            if(defined(newScope.$shared.$g.closeOnBlur))
                return newScope.$shared.$g.closeOnBlur();
            else
                return true;
        }


        // Show popup
        $dom.pop({
            scope: newScope,
            template: '<simple-popup><div data-ng-controller="'+controller+'" data-load-template="templateUrl"></div></simple-popup>'
        }, {
            closeOnBlur: shouldCloseOnBlur,
            target: srcEl,
            place: 'r',
            anchor: 't',
            minWidth: 428,
            minHeight: 356,
            space: 11,
            css: 'uc-db-popup'
        });

        return cancelCore;
    }

	//////////////////////////////////
	//	Initialization Functions	//
	//////////////////////////////////

	// Registering dashboard/ widget creation in order to perform drilling
	prism.on("dashboardloaded", function (e, args) {
		args.dashboard.on("widgetinitialized", onWidgetInitialized);
	});

	// register widget events upon initialization
	function onWidgetInitialized(dashboard, args) {
		//	Hooking to ready/destroyed events
		args.widget.on("destroyed", onWidgetDestroyed);		
		//	Add hook for the time formatting
		var shouldInit = widgetIsSupported(args.widget.type);
		if (shouldInit) {
			args.widget.on("buildquery", updateQuery);
		}
	}

	// unregistering widget events
	function onWidgetDestroyed(widget, args) {
		widget.off("destroyed", onWidgetDestroyed);
	}

	//	Create Options for the quadrant analysis
	prism.on("beforemenu", function (scope, args) {

		//	Can we show the option?	
		var shouldEnable = canEnable(scope, args);				

		//	Add the option to enable the regression line
		if (shouldEnable) {			
			
			//	Get the item and widget
			var selectedItem = args.settings.item,
				widget = scope.currentScope.widget;

			//	Look for a saved setting
			var mySetting = widget.options[myProperty] ? widget.options[myProperty] : null,
				pickerCaption = mySetting ? mySetting.jaql.title : noFilterCaption;

			//	Function the runs when an item is picked
			var dimSelector = function(){

				showPicker(scope, widget, this.clickedElement[0], this.item);

				/*

				//	Create a copy of the scope
				var newScope = scope.targetScope.$new();

				//	Add custom attributes to the new scope
	            newScope.name = "af-" + this.item.jaql.title;
	            newScope.item = this.item;
	            newScope.widget = widget
	            newScope.isFiltersPanel = false;
	            newScope.isCustomPopup = true;
	            newScope.isMeasure = false;


	            //	Define what field types are displayed
	            var ualTypes = { 
	            	dimensions: true,
	            	measures: false 
	            };

	            //	Existing Jaql to pass in (used when adding custom jaql to filters)
	            var existingJaql = null;

	            //	triggering element event
	            var eventElement = this.clickedElement[0];

	            //	callback function for pre-cancel
	            var userCanceled = function () {
	            
	                //	Apply the scope
	                if (!scope.targetScope.$$phase){
	                	scope.targetScope.$apply()
	                }
	            }

	            //	Callback function after selection
	            var fieldSelected = function (newjaql) {
	            	debugger
	                if (period == 'color') {
	                    setFieldsArr(widgetOptions.colorFields,newjaql);
	                } else {
	                    setFieldsArr(widgetOptions.sizeFields,newjaql);
	                }

	                $(".uc-db-popup.sis-popup-window.popup-host.simple").remove();
	                $(".popup-overlay").remove();

	                //$scope.widget.changesMade();
	                $scope.widget.redraw();
	            }

	            //	Title key for localized label lookup
	            var titleKey = 'we.breads.addfield';

	            //	Instantiate the popup
	            var close = popJaql(newScope, existingJaql, eventElement, userCanceled, null, fieldSelected, false, ualTypes);
				*/
			}

			//	Function to clear an existing filter
			var clearFilter = function(){

			}

			//	Create settings menu
			var separator = {
				type: 'separator'
			};
			var newItem = {
				caption: headerMenuCaption,
				items: [
					{
						caption: pickerCaption,
						size: 'xl',
						type: 'option',
						widget: widget,
						item: args.settings.item,
						execute: dimSelector,
						clickedElement: args.ui.target,
						closing: true
					}
				]
			};

			//	Only add the clear button if the user has already added a filter
			if (mySetting){
				//	Create the clear button
				var clearItem = {
					caption: clearFilterCaption,
					size: 'xl',
					type: 'check',
					widget: widget,
					execute: clearFilter,
					confirm: {
						critical: false,
						message: "Are you sure you want to remove the filter?"
					}
				}
				//	Add to the menu
				newItem.items.push(clearItem);
			}

			//	Add options to the menu			
			args.settings.items.push(separator);	
			args.settings.items.push(newItem);	
		}
		
	});

}]);